﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}
		List<string> listaRijeci = new List<string>();
		Game i;
		private void Form1_Load(object sender, EventArgs e)
		{
			using (System.IO.StreamReader reader = new System.IO.StreamReader("RijeciZaIgru.txt"))
			{
				string linija;
				linija = reader.ReadLine();
				string[] parts = linija.Split(',');
				foreach (string s in parts)
					listaRijeci.Add(s);
			}
			Random rnd = new Random();
			int n = rnd.Next(0, listaRijeci.Count - 1);
			string clue = listaRijeci[n];
			i = new Game(clue);
			i.Ispis();
			labelWord.Text = i.forLabel;
			labelLifeCount.Text = i.BrojZivota.ToString();
		}
		public class Game
		{
			public int BrojZivota { get; private set; }
			public string rijec { get; private set; }
			public List<char> Pokusaji { get; set; }
			private string IsprobanaSlova = String.Empty;
			public int BrojIsprobanihSlova { get; private set; }
			public string forLabel { get; private set; }
			public Game(string r)
			{
				rijec = r;
				BrojZivota = 6;
				Pokusaji = new List<char>();
				BrojIsprobanihSlova = 0;
			}
			public bool Pronadeno(char c)
			{
				foreach (char d in IsprobanaSlova)
				{
					if (d == c) return true;
				}
				return false;
			}
			public void Ispis()
			{
				forLabel = String.Empty;
				foreach (char c in rijec)
				{
					if (c == ' ') forLabel += c;
					else if (Pronadeno(c)) forLabel += c;
					else forLabel += '_';
					forLabel += ' ';
				}
			}
			public void Guess(char c)
			{
				c = char.ToUpper(c);
				Pokusaji.Add(c);
				foreach (char d in rijec)
				{
					if (d == c)
					{
						BrojIsprobanihSlova++;
						IsprobanaSlova += c;
					}
				}
				if (!Pronadeno(c)) BrojZivota--;
			}
			public int spaceCount()
			{
				int space = 0;
				foreach (char c in rijec)
					if (c == ' ') space++;
				return space;
			}
			public bool AlreadyTried(char c)
			{
				foreach (char d in Pokusaji)
					if (c == d) return true;
				return false;
			}
		}

		private void buttonPogadjaj_Click(object sender, EventArgs e)
		{
			if (textBoxSlovo.Text == String.Empty) MessageBox.Show("Niste unijeli slovo!", "Pogreška");
			else if (i.AlreadyTried(char.ToUpper(textBoxSlovo.Text[0]))) { MessageBox.Show("Već ste pogađali to slovo!", "Pogreška"); textBoxSlovo.Text = String.Empty; }
			else
			{
				i.Guess(textBoxSlovo.Text[0]);
				i.Ispis();
				labelWord.Text = i.forLabel;
				labelLifeCount.Text = i.BrojZivota.ToString();
				textBoxSlovo.Text = String.Empty;
				if (i.BrojZivota == 0)
				{
					MessageBox.Show("Izgubili ste nazalost!");
					labelWord.Text = i.rijec;
					labelUnos.Visible = false;
					textBoxSlovo.Visible = false;
					buttonPogadjaj.Visible = false;
					Application.Exit();
				}
				else if (i.BrojIsprobanihSlova + i.spaceCount() == i.rijec.Length)
				{
					MessageBox.Show("Pobijedili ste, cestitam!");
					buttonPogadjaj.Visible = false;
					labelUnos.Visible = false;
					textBoxSlovo.Visible = false;
					Application.Exit();
				}
			}
		}
	}
}
