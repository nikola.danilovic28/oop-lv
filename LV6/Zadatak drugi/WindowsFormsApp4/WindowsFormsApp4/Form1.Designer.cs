﻿namespace WindowsFormsApp4
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.labelWord = new System.Windows.Forms.Label();
            this.labelPP = new System.Windows.Forms.Label();
            this.labelLifeCount = new System.Windows.Forms.Label();
            this.labelUnos = new System.Windows.Forms.Label();
            this.textBoxSlovo = new System.Windows.Forms.TextBox();
            this.buttonPogadjaj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelWord
            // 
            this.labelWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWord.Location = new System.Drawing.Point(0, 0);
            this.labelWord.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelWord.Name = "labelWord";
            this.labelWord.Size = new System.Drawing.Size(668, 193);
            this.labelWord.TabIndex = 0;
            this.labelWord.Text = "label1";
            this.labelWord.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelPP
            // 
            this.labelPP.AutoSize = true;
            this.labelPP.Location = new System.Drawing.Point(113, 107);
            this.labelPP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPP.Name = "labelPP";
            this.labelPP.Size = new System.Drawing.Size(101, 13);
            this.labelPP.TabIndex = 1;
            this.labelPP.Text = "Preostali broj zivota:";
            // 
            // labelLifeCount
            // 
            this.labelLifeCount.AutoSize = true;
            this.labelLifeCount.Location = new System.Drawing.Point(240, 107);
            this.labelLifeCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelLifeCount.Name = "labelLifeCount";
            this.labelLifeCount.Size = new System.Drawing.Size(35, 13);
            this.labelLifeCount.TabIndex = 2;
            this.labelLifeCount.Text = "label3";
            // 
            // labelUnos
            // 
            this.labelUnos.AutoSize = true;
            this.labelUnos.Location = new System.Drawing.Point(113, 152);
            this.labelUnos.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelUnos.Name = "labelUnos";
            this.labelUnos.Size = new System.Drawing.Size(143, 13);
            this.labelUnos.TabIndex = 3;
            this.labelUnos.Text = "Pogodi slovo koje nedostaje!";
            // 
            // textBoxSlovo
            // 
            this.textBoxSlovo.Location = new System.Drawing.Point(350, 149);
            this.textBoxSlovo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxSlovo.MaxLength = 1;
            this.textBoxSlovo.Name = "textBoxSlovo";
            this.textBoxSlovo.Size = new System.Drawing.Size(19, 20);
            this.textBoxSlovo.TabIndex = 4;
            // 
            // buttonPogadjaj
            // 
            this.buttonPogadjaj.Location = new System.Drawing.Point(547, 146);
            this.buttonPogadjaj.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPogadjaj.Name = "buttonPogadjaj";
            this.buttonPogadjaj.Size = new System.Drawing.Size(58, 24);
            this.buttonPogadjaj.TabIndex = 5;
            this.buttonPogadjaj.Text = "Probaj!";
            this.buttonPogadjaj.UseVisualStyleBackColor = true;
            this.buttonPogadjaj.Click += new System.EventHandler(this.buttonPogadjaj_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 193);
            this.Controls.Add(this.buttonPogadjaj);
            this.Controls.Add(this.textBoxSlovo);
            this.Controls.Add(this.labelUnos);
            this.Controls.Add(this.labelLifeCount);
            this.Controls.Add(this.labelPP);
            this.Controls.Add(this.labelWord);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Vješala";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelWord;
		private System.Windows.Forms.Label labelPP;
		private System.Windows.Forms.Label labelLifeCount;
		private System.Windows.Forms.Label labelUnos;
		private System.Windows.Forms.TextBox textBoxSlovo;
		private System.Windows.Forms.Button buttonPogadjaj;
	}
}

