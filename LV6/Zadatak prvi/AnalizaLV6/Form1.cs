﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnalizaLV6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double br1, br2;

        private void br1_txtbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void razlika_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos prvog operanda!");
            else if (!double.TryParse(br2_txtbox.Text, out br2))
                MessageBox.Show("Pogresan unos drugog operanda!");
            else
            {
                rez_lbl.Text = "Rezultat je: " + (br1 - br2).ToString();
            }
        }

        private void produkt_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos prvog operanda!");
            else if (!double.TryParse(br2_txtbox.Text, out br2))
                MessageBox.Show("Pogresan unos drugog operanda!");
            else
            {
                rez_lbl.Text = "Rezultat je: " + (br1 * br2).ToString();
            }
        }

        private void kvocjent_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos prvog operanda!");
            else if (!double.TryParse(br2_txtbox.Text, out br2))
                MessageBox.Show("Pogresan unos drugog operanda!");
            else if (br2 == 0)
            {
                MessageBox.Show("Ne moze se dijeliti s nulom!");
            }
            else
            {
                rez_lbl.Text = "Rezultat je: " + (br1 / br2).ToString();
            }
        }

        private void sin_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos operanda!");
            else
            {
                rez_lbl.Text = "Rezultat je: " + (Math.Sin(br1)).ToString();
            }
        }

        private void cos_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos operanda!");
            else
            {
                rez_lbl.Text = "Rezultat je: " + (Math.Cos(br1)).ToString();
            }
        }

        private void log_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos operanda!");
            else
            {
                rez_lbl.Text = "Rezultat je: " + (Math.Log10(br1)).ToString();
            }
        }

        private void ln_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos operanda!");
            else
            {
                rez_lbl.Text = "Rezultat je: " + (Math.Log(br1)).ToString();
            }
        }

        private void sqrt_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos operanda!");
            else
            {
                rez_lbl.Text = "Rezultat je: " + (Math.Sqrt(br1)).ToString();
            }
        }

        private void Izlaz_btn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void zbroj_btn_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(br1_txtbox.Text, out br1))
                MessageBox.Show("Pogresan unos prvog operanda!");
            else if (!double.TryParse(br2_txtbox.Text, out br2))
                MessageBox.Show("Pogresan unos drugog operanda!");
            else
            {
                rez_lbl.Text = "Rezultat je: " + (br1 + br2).ToString();
            }
        }

    }
}
