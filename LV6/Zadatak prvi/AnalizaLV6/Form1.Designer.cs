﻿namespace AnalizaLV6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zbroj_btn = new System.Windows.Forms.Button();
            this.razlika_btn = new System.Windows.Forms.Button();
            this.produkt_btn = new System.Windows.Forms.Button();
            this.cos_btn = new System.Windows.Forms.Button();
            this.log_btn = new System.Windows.Forms.Button();
            this.kvocjent_btn = new System.Windows.Forms.Button();
            this.ln_btn = new System.Windows.Forms.Button();
            this.sin_btn = new System.Windows.Forms.Button();
            this.sqrt_btn = new System.Windows.Forms.Button();
            this.br1_txtbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.br2_txtbox = new System.Windows.Forms.TextBox();
            this.rez_lbl = new System.Windows.Forms.Label();
            this.Izlaz_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // zbroj_btn
            // 
            this.zbroj_btn.Location = new System.Drawing.Point(327, 15);
            this.zbroj_btn.Name = "zbroj_btn";
            this.zbroj_btn.Size = new System.Drawing.Size(62, 23);
            this.zbroj_btn.TabIndex = 1;
            this.zbroj_btn.Text = "zbroj";
            this.zbroj_btn.UseVisualStyleBackColor = true;
            this.zbroj_btn.Click += new System.EventHandler(this.zbroj_btn_Click);
            // 
            // razlika_btn
            // 
            this.razlika_btn.Location = new System.Drawing.Point(327, 44);
            this.razlika_btn.Name = "razlika_btn";
            this.razlika_btn.Size = new System.Drawing.Size(62, 23);
            this.razlika_btn.TabIndex = 2;
            this.razlika_btn.Text = "razlika";
            this.razlika_btn.UseVisualStyleBackColor = true;
            this.razlika_btn.Click += new System.EventHandler(this.razlika_btn_Click);
            // 
            // produkt_btn
            // 
            this.produkt_btn.Location = new System.Drawing.Point(327, 73);
            this.produkt_btn.Name = "produkt_btn";
            this.produkt_btn.Size = new System.Drawing.Size(62, 23);
            this.produkt_btn.TabIndex = 3;
            this.produkt_btn.Text = "produkt";
            this.produkt_btn.UseVisualStyleBackColor = true;
            this.produkt_btn.Click += new System.EventHandler(this.produkt_btn_Click);
            // 
            // cos_btn
            // 
            this.cos_btn.Location = new System.Drawing.Point(327, 184);
            this.cos_btn.Name = "cos_btn";
            this.cos_btn.Size = new System.Drawing.Size(62, 23);
            this.cos_btn.TabIndex = 4;
            this.cos_btn.Text = "cos";
            this.cos_btn.UseVisualStyleBackColor = true;
            this.cos_btn.Click += new System.EventHandler(this.cos_btn_Click);
            // 
            // log_btn
            // 
            this.log_btn.Location = new System.Drawing.Point(327, 213);
            this.log_btn.Name = "log_btn";
            this.log_btn.Size = new System.Drawing.Size(62, 23);
            this.log_btn.TabIndex = 5;
            this.log_btn.Text = "log";
            this.log_btn.UseVisualStyleBackColor = true;
            this.log_btn.Click += new System.EventHandler(this.log_btn_Click);
            // 
            // kvocjent_btn
            // 
            this.kvocjent_btn.Location = new System.Drawing.Point(327, 102);
            this.kvocjent_btn.Name = "kvocjent_btn";
            this.kvocjent_btn.Size = new System.Drawing.Size(62, 23);
            this.kvocjent_btn.TabIndex = 6;
            this.kvocjent_btn.Text = "kvocjent";
            this.kvocjent_btn.UseVisualStyleBackColor = true;
            this.kvocjent_btn.Click += new System.EventHandler(this.kvocjent_btn_Click);
            // 
            // ln_btn
            // 
            this.ln_btn.Location = new System.Drawing.Point(327, 242);
            this.ln_btn.Name = "ln_btn";
            this.ln_btn.Size = new System.Drawing.Size(62, 23);
            this.ln_btn.TabIndex = 7;
            this.ln_btn.Text = "ln";
            this.ln_btn.UseVisualStyleBackColor = true;
            this.ln_btn.Click += new System.EventHandler(this.ln_btn_Click);
            // 
            // sin_btn
            // 
            this.sin_btn.Location = new System.Drawing.Point(327, 155);
            this.sin_btn.Name = "sin_btn";
            this.sin_btn.Size = new System.Drawing.Size(62, 23);
            this.sin_btn.TabIndex = 8;
            this.sin_btn.Text = "sin";
            this.sin_btn.UseVisualStyleBackColor = true;
            this.sin_btn.Click += new System.EventHandler(this.sin_btn_Click);
            // 
            // sqrt_btn
            // 
            this.sqrt_btn.Location = new System.Drawing.Point(327, 271);
            this.sqrt_btn.Name = "sqrt_btn";
            this.sqrt_btn.Size = new System.Drawing.Size(62, 23);
            this.sqrt_btn.TabIndex = 9;
            this.sqrt_btn.Text = "sqrt";
            this.sqrt_btn.UseVisualStyleBackColor = true;
            this.sqrt_btn.Click += new System.EventHandler(this.sqrt_btn_Click);
            // 
            // br1_txtbox
            // 
            this.br1_txtbox.Location = new System.Drawing.Point(12, 44);
            this.br1_txtbox.Name = "br1_txtbox";
            this.br1_txtbox.Size = new System.Drawing.Size(195, 20);
            this.br1_txtbox.TabIndex = 11;
            this.br1_txtbox.TextChanged += new System.EventHandler(this.br1_txtbox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(309, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "1. broj binarne operacije i broj nad kojim se radi unarna operacija";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "2. broj binarne operacije";
            // 
            // br2_txtbox
            // 
            this.br2_txtbox.Location = new System.Drawing.Point(12, 131);
            this.br2_txtbox.Name = "br2_txtbox";
            this.br2_txtbox.Size = new System.Drawing.Size(195, 20);
            this.br2_txtbox.TabIndex = 14;
            // 
            // rez_lbl
            // 
            this.rez_lbl.AutoSize = true;
            this.rez_lbl.Location = new System.Drawing.Point(12, 194);
            this.rez_lbl.Name = "rez_lbl";
            this.rez_lbl.Size = new System.Drawing.Size(60, 13);
            this.rez_lbl.TabIndex = 15;
            this.rez_lbl.Text = "Rezultat je:";
            // 
            // Izlaz_btn
            // 
            this.Izlaz_btn.Location = new System.Drawing.Point(12, 322);
            this.Izlaz_btn.Name = "Izlaz_btn";
            this.Izlaz_btn.Size = new System.Drawing.Size(377, 23);
            this.Izlaz_btn.TabIndex = 16;
            this.Izlaz_btn.Text = "Izlaz";
            this.Izlaz_btn.UseVisualStyleBackColor = true;
            this.Izlaz_btn.Click += new System.EventHandler(this.Izlaz_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 357);
            this.Controls.Add(this.Izlaz_btn);
            this.Controls.Add(this.rez_lbl);
            this.Controls.Add(this.br2_txtbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.br1_txtbox);
            this.Controls.Add(this.sqrt_btn);
            this.Controls.Add(this.sin_btn);
            this.Controls.Add(this.ln_btn);
            this.Controls.Add(this.kvocjent_btn);
            this.Controls.Add(this.log_btn);
            this.Controls.Add(this.cos_btn);
            this.Controls.Add(this.produkt_btn);
            this.Controls.Add(this.razlika_btn);
            this.Controls.Add(this.zbroj_btn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button zbroj_btn;
        private System.Windows.Forms.Button razlika_btn;
        private System.Windows.Forms.Button produkt_btn;
        private System.Windows.Forms.Button cos_btn;
        private System.Windows.Forms.Button log_btn;
        private System.Windows.Forms.Button kvocjent_btn;
        private System.Windows.Forms.Button ln_btn;
        private System.Windows.Forms.Button sin_btn;
        private System.Windows.Forms.Button sqrt_btn;
        private System.Windows.Forms.TextBox br1_txtbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox br2_txtbox;
        private System.Windows.Forms.Label rez_lbl;
        private System.Windows.Forms.Button Izlaz_btn;
    }
}

